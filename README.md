# Spletni barvni stroboskop

1\. domača naloga pri predmetu [Osnove informacijskih sistemov](https://ucilnica.fri.uni-lj.si/course/view.php?id=54) (navodila)


## 6.1 Opis naloge in navodila

Na Bitbucket je na voljo javni repozitorij [https://bitbucket.org/asistent/stroboskop](https://bitbucket.org/asistent/stroboskop), ki vsebuje nedelujoč spletni stroboskop. V okviru domače naloge ustvarite kopijo repozitorija ter popravite in dopolnite obstoječo implementacijo spletne strani tako, da bo končna aplikacija z vsemi delujočimi funkcionalnostimi izgledala kot na spodnji sliki. Natančna navodila si preberite na [spletni učilnici](https://ucilnica.fri.uni-lj.si/mod/workshop/view.php?id=19181).

![Stroboskop](stroboskop.gif)

## 6.2 Vzpostavitev repozitorija

Naloga 6.2.2:

```
git clone git@bitbucket.org:maver42/stroboskop.git
mv stroboskop dn
cd dn
```

Naloga 6.2.3:
https://bitbucket.org/maver42/stroboskop/commits/eaf5c300af4bd5308ab3804cba559e9501686fc4

## 6.3 Skladnja strani in stilska preobrazba

Naloga 6.3.1:
https://bitbucket.org/maver42/stroboskop/commits/4f097abc5f276343348cbf73045dbc0e890cc0ae

Naloga 6.3.2:
https://bitbucket.org/maver42/stroboskop/commits/89e0f238acb5e3634c172b039a131a2ca03b2fd9

Naloga 6.3.3:
https://bitbucket.org/maver42/stroboskop/commits/c0922787abfa46d9edecbc85bfda8e609f050092

Naloga 6.3.4:
https://bitbucket.org/maver42/stroboskop/commits/e1398b6a83897e0c6990121bdd1eb311e562c170

Naloga 6.3.5:

```
git checkout master
git merge izgled
```

## 6.4 Dinamika in animacija na strani

Naloga 6.4.1:
https://bitbucket.org/maver42/stroboskop/commits/a62d8f60a5e6d5e397c0b09a69e8aaf2ace949be

Naloga 6.4.2:
https://bitbucket.org/maver42/stroboskop/commits/2ef4b4679166e0b6e3597ed2274137b7150b1f9a

Naloga 6.4.3:
https://bitbucket.org/maver42/stroboskop/commits/0467069bc34728809b694ce007439fd4a87ef3a8

Naloga 6.4.4:
https://bitbucket.org/maver42/stroboskop/commits/0a8a2848cbd76239da34586685397d1600c8f571